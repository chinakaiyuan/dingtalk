<?php
/*
 * @Author: sunkaiyuan 
 * @Date: 2021-11-17 14:56:33 
 * @Last Modified by: sunkaiyuan
 * @Last Modified time: 2021-11-17 17:09:35
 */

namespace EasyDingTalk\Messages;

class MarkDown extends Message
{
    protected $type = 'markdown';
}
