<?php

/*
 * This file is part of the mingyoung/dingtalk.
 *
 * (c) 张铭阳 <mingyoungcheung@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyDingTalk\Im;

use EasyDingTalk\Kernel\Client as BaseClient;

class Client extends BaseClient
    {
    public function sendCards($params, $id, $robotCode = '7cc3e49d2c71bab14a352500212932a0903641dee59720e3d35fbf54ebbfd087', $cardTemplateId = '8375de13-e427-454d-aa7b-b6d615bd8afc.schema')
        {
        $data = [
            'cardTemplateId'          => $cardTemplateId,
            'outTrackId'              => $id,//消息ID
            'openSpaceId'             => $robotCode,
            'cardData'                => ['cardParamMap' => $params],
            'imGroupOpenDeliverModel' => [
                'robotCode' => $robotCode
            ]
        ];

        $this->uriVersion = 'v1';
        return $this->postJson('/v1.0/card/instances/createAndDeliver', $data);
        }
    }
